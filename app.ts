import express, { Request } from 'express';
import createError from 'http-errors';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import http from 'http';
import debug from "debug";
import passport from "passport";
import cors from 'cors';
import dotenv from 'dotenv';
dotenv.config();

import router from './routes/index';
import { JWTStrategy } from './utils/jwt-strategy';
import LocalStrategy from './utils/local_strategy';
import AdminStrategy from './utils/admin_strategy';
import { configureMongo } from './utils/mongo_connect';
declare global {
  namespace NodeJS {
    interface Global {
      appRoot : string
    }
  }
}

declare module 'express-serve-static-core' {
  interface Request { 
    user : {
      _id : string
    }
  }
}

const app = express();
const port = parseInt(process.env.PORT) || 5000 ;


global.appRoot = path.resolve(__dirname);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.set('view engine', 'pug')
app.use(cors());
app.use(logger('dev'));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use(passport.initialize())
passport.use(JWTStrategy());
passport.use(LocalStrategy());
passport.use('admin', AdminStrategy());
app.use('/api', router);
// app.use(passport.initialize());

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

const server = http.createServer(app);
server.listen(port, () => {
  console.log("Server listening on port : " + port);
  configureMongo();
});
app.on('error', onError);


function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}