import { Document, Model, model, Schema } from "mongoose";

interface AccountDoc extends Document {
    username : string;
    email : string
    name : string;
    verified : boolean;
    locked : boolean;
    type: string;
}

interface AccountModel extends Model<AccountDoc> {}

const AccountSchema = new Schema<AccountDoc>({
    username : {
        type : String,
        length : 255
    },
    email : {
        type : String,
        unique : true,
        length : 255
    },
    name : {
        type : String,
        length : 255
    },
    password : {
        type : String,
        length : 255
    },
    verified : Boolean,
    locked : Boolean,
    type: String
}, {
    toJSON : {
        transform(_, ret){
            ret.id = ret._id;
            delete ret._id;
            delete ret._v;
            delete ret.password;
            delete ret.salt;
        }
    }
})

const Account = model<AccountDoc, AccountModel>('Account', AccountSchema);

export default Account;