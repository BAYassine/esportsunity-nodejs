import { Document, Model, model, Schema } from "mongoose";

export interface GameDoc extends Document {
    name : string,
    developer : string,
    api : string,
    foundation : Date,
    overview : string,
    icon : string,
    cover : string,
    portrait : string,
    transparen : string,
    nickname_label : string,
    rank_label : string,
    website : string,
    categories : String[]
}

interface GameModel extends Model<GameDoc> {}

const GamesSchema = new Schema<GameDoc>({
    name : {
        type : String,
        length : 255
    },
    developer : {
        type : String,
        length : 255
    },
    api : {
        name: String,
        token : String
    },
    foundation : Date,
    overview : {
        type : String,
        length : 1000
    }, 
    icon : {
        type: String,
        length : 255
    },
    cover : {
        type: String,
        length : 255
    },
    portrait : {
        type: String,
        length : 255
    },
    transparent : {
        type: String,
        length : 255
    },
    nickname_label : {
        type: String,
        length : 255
    },
    rank_label : {
        type: String,
        length : 255
    },
    website : {
        type: String,
        length : 255
    },
    categories : [{
        type : String
    }]
}, {
    toJSON : {
        transform(_, ret){
            ret.id = ret._id;
            delete ret._id;
        }
    }
});

const Game = model<GameDoc, GameModel>('Game', GamesSchema);

export default Game;