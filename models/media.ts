import { Document, model, Model, Schema, Types } from "mongoose";
export interface MediaDoc extends Document {
    name : string;
    type : string;
    used : boolean;
}

interface MediaModel extends Model<MediaDoc> {}

const MediaSchema = new Schema<MediaDoc>({
    name : {
        type : String,
        index : true
    },
    type : String,
    used : Boolean
}, {
    toJSON : {
        transform(_, ret){
            ret.id = ret._id;
            delete ret._id;
            delete ret._v;
        }
    },
    timestamps : true
})

const Media = model<MediaDoc, MediaModel>('Media', MediaSchema);

export default Media;