import { NewsSectionTypes } from "../utils/constants";


export abstract class NewsSection {
    protected type : NewsSectionTypes
}

export class GroupSection extends NewsSection {
    type : NewsSectionTypes.GROUP;
    alignment : "V" | "H";
    sections? : NewsSection[];
}

export class ParagraphSection extends NewsSection {
    
    type = NewsSectionTypes.PARAGRAPH;
    title? : string;
    content : string;

}

export class MediaSection extends NewsSection {

    type = NewsSectionTypes.MEDIA;
    mediatype : "VIDEO" | "IMAGE";
    title : string;
    source : string;
    link : string;

}

export class QuoteSection extends NewsSection {

    type = NewsSectionTypes.QUOTE;
    quote : string;
    said_by : string;
    date : Date;
    link : string;

}

export class BriefingSection extends NewsSection {

    type = NewsSectionTypes.BRIEFING;
    content : string;

}