import { Document, model, Model, Schema, Types, Mixed } from "mongoose";
import { NewsCategories } from "../utils/constants";
import { NewsSection } from "./news-sections";

export interface NewsDoc extends Document {
    title : string;
    cover : string;
    featured : boolean;
    category : NewsCategories,
    briefing : string;
    source : string;
    published : boolean;
    sections : NewsSection[];
    createdBy : string;
    author : string;
    publishedOn : Date;
}

interface NewsModel extends Model<NewsDoc> {}

const NewsSchema = new Schema<NewsDoc>({
    title : String,
    cover : String,
    category : {
        type : String,
        enum : Object.keys(NewsCategories),
        length : 255
    },
    featured : Boolean,
    briefing : String,
    sections : [],
    source : String,
    author : String,
    publishedOn : Date,
    createdBy : { type: Types.ObjectId, ref: 'StaffAccount' }
}, {
    toJSON : {
        transform(_, ret){
            ret.id = ret._id;
            delete ret._id;
            delete ret._v;
        }
    },
    timestamps : true
})

const News = model<NewsDoc, NewsModel>('News', NewsSchema);

export default News;