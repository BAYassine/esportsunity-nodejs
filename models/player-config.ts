import { model, Model, Schema, Types, Document } from "mongoose";
export interface ConfigDoc extends Document {
    account_id : string;
    mouse : string;
    keyboard : string;
    screen : string;
    graphics : string;
    processor : string;
    memory : number;
    chair : string;
    headphones : string;
    mousepad : string;
}

interface ConfigModel extends Model<ConfigDoc> {}

const ConfigSchema = new Schema<ConfigDoc>({
    account_id : {
        type : Types.ObjectId,
    },
    mouse : String,
    keyboard : String,
    screen : String,
    graphics : String,
    processor : String,
    memory : Number,
    chair : String,
    headphones : String,
    mousepad : String,
}, {
    toJSON : {
        transform(_, ret){
            ret.id = ret._id;
            delete ret._id;
            delete ret._v;
        }
    }
})

const Config = model<ConfigDoc, ConfigModel>('Config', ConfigSchema);

export default Config;