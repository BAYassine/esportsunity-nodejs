import { model, Model, Document, Types, Schema } from "mongoose";

export interface PlayerDoc extends Document {
    account_id : string,
    nickname : string,
    about : string,
    firstname : string,
    lastname : string,
    age : number,
    nationality : string,
    avatar : string,
    photo : string,
    website : string
    social_links : {
        id: string,
        social_network : string,
        link : string
    }[]
}

interface PlayerModel extends Model<PlayerDoc> {}

const PlayerSchema = new Schema<PlayerDoc>({
    account_id : {
        type : Types.ObjectId,
    },
    nickname : {
        type : String,
        length : 255
    },
    about : {
        type : String,
        length : 10000
    },
    firstname : {
        type : String,
        length : 255
    },
    lastname : {
        type : String,
        length : 255
    },
    age : Number,
    nationality : {
        type : String,
        length : 255
    },
    avatar : {
        type : String,
        length : 255
    },
    photo : {
        type : String,
        length : 255
    },
    website : {
        type : String,
        length : 255
    },
    social_links : [{
        social_network : String,
        link : String
    }]
}, {
    toJSON : {
        transform(_, ret){
            ret.id = ret._id;
            delete ret._id;
            delete ret._v;
        }
    }
})

const Player = model<PlayerDoc, PlayerModel>('Player', PlayerSchema);

export default Player;