import { Document, Schema } from "mongoose";

interface PrizeDoc extends Document {
    label : string;
    from : number;
    to : number;
    photo : string;
}

const PrizeSchema = new Schema<PrizeDoc>(
    {
        label: String,
        from: Number,
        to: Number,
        photo : String
    },
    {
        toJSON: {
            transform(_, ret) {
                ret.id = ret._id;
                delete ret._id;
                delete ret._v;
            }
        }
    })

export { PrizeDoc, PrizeSchema };