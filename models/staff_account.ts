import { Document, Model, model, Schema } from "mongoose";

export interface StaffAccountDoc extends Document {
    username : string;
    email : string;
    name : string;
    photo : string;
    password : string;
    verified : boolean;
    locked : boolean;
    roles: string[];
}

interface StaffAccountModel extends Model<StaffAccountDoc> {}

const StaffAccountSchema = new Schema<StaffAccountDoc>({
    username : {
        type : String,
        length : 255
    },
    email : {
        type : String,
        length : 255
    },
    name : {
        type : String,
        length : 255
    },
    photo : {
        type : String,
        length : 255
    },
    password : {
        type : String,
        length : 255
    },
    verified : Boolean,
    locked : Boolean,
    roles: [String]
}, {
    toJSON : {
        transform(_, ret){
            ret.id = ret._id;
            delete ret._id;
            delete ret.password;
        }
    }
})

const StaffAccount = model<StaffAccountDoc, StaffAccountModel>('StaffAccount', StaffAccountSchema);

export default StaffAccount;