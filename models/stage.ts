import { Document, Model, Schema } from "mongoose";
import { StageFormats } from "../utils/constants";

interface StageDoc extends Document {
    name: string;
    format: StageFormats;
    startDate: Date;
    endDate: Date;
    order: number;
    rounds: number;
    current : boolean;
}

const StageSchema = new Schema<StageDoc>(
    {
        name: String,
        format: String,
        startDate: Date,
        endDate: Date,
        order: Number,
        rounds: Number,
        current : Boolean
    },
    {
        toJSON: {
            transform(_, ret) {
                ret.id = ret._id;
                delete ret._id;
                delete ret._v;
            }
        }
    })

export { StageDoc, StageSchema };