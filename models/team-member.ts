import mongoose, { model, Schema } from "mongoose";
import { PlayerDoc } from "./player";
import { TeamDoc } from "./team";

export enum TeamMemberRoles {
    CAPTAIN = "CAPTAIN",
    COACH = "COACH",
    PLAYER = "PLAYER"
}


export interface TeamMemberDoc extends mongoose.Document {
    player : PlayerDoc;
    team : TeamDoc;
    joinDate : Date;
    role : TeamMemberRoles;
    photo : string;
}

interface TeamMemberModel extends mongoose.Model<TeamMemberDoc> {}

const TeamSchema = new Schema<TeamMemberDoc>({
    team: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Team',
    },
    player: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Player',
    },
    joinDate : Date,
    role : String,
    photo : String,
}, {
    toJSON : {
        transform(_, ret){
            ret.id = ret._id;
            delete ret._id;
        }
    }
});

const TeamMember = model<TeamMemberDoc, TeamMemberModel>('TeamMember', TeamSchema);

export default TeamMember;