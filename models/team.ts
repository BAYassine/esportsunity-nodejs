import { Document, Model, model, Types, Schema } from "mongoose";

export interface TeamDoc extends Document {
    captain_id : string;
    byAdmin : boolean;
    name : string;
    members : number;
    tag : string;
    country : string;
    coach : string;
    ceo : string;
    foundation: Date;
    about : string;
    logo : string;
    cover : string;
    website : string;
    photo : string;
}

interface TeamModel extends Model<TeamDoc> {}

const TeamSchema = new Schema<TeamDoc>({
    captain_id : Types.ObjectId,
    byAdmin : Boolean,
    name : {
        type : String,
        length : 255
    },
    members : {
        type : Number
    },
    tag : {
        type : String,
        length : 20,
    },
    country : {
        type : String,
        length : 255
    },
    ceo : {
        type : String,
        length : 255
    },
    coach : {
        type : String,
        length : 255
    },
    foundation : Date,
    about : {
        type : String,
        length : 1000
    }, 
    logo : {
        type: String,
        length : 255
    },
    cover : {
        type: String,
        length : 255
    },
    photo : {
        type: String,
        length : 255
    },
    website : {
        type: String,
        length : 255
    }
}, {
    timestamps : true,
    toJSON : {
        transform(_, ret){
            ret.id = ret._id;
            delete ret._id;
        }
    }
});

const Team = model<TeamDoc, TeamModel>('Team', TeamSchema);

export default Team;