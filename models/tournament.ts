import { Document, model, Model, Schema, Types } from "mongoose";
import { TournamentStatus } from "../utils/constants";
import { PrizeDoc, PrizeSchema } from "./prize";
import { StageSchema, StageDoc } from "./stage";

export interface TournamentDoc extends Document{
    name : string,
    organizer : string,
    overview : string,
    cover : string,
    startDate : Date,
    endDate : Date,
    entryClosesOn : Date,
    publishedOn : Date,
    places : number,
    status : TournamentStatus,
    teamsize : number,
    entryFee : number,
    region : string,
    participation : "TEAM" | "INDIVIDUAL",
    type : "COMMUNITY" | "OFFICIAL",
    stages : StageDoc[],
    prizes : PrizeDoc[],
    game : string
}

interface TournamentModel extends Model<TournamentDoc> {}

const TournamentSchema = new Schema<TournamentDoc>({
    name : String,
    organizer : String,
    overview : String,
    cover : String,
    startDate : Date,
    endDate : Date,
    entryClosesOn : Date,
    publishedOn : Date,
    places : Number,
    type : String,
    status : Object.values(TournamentStatus),
    teamsize : Number,
    entryFee : Number,
    region : String,
    participation : String,
    stages : [StageSchema],
    prizes : [PrizeSchema],
    game : { type: Types.ObjectId, ref: 'Game' }
}, {
    toJSON : {
        transform(_, ret){
            ret.id = ret._id;
            delete ret._id;
            delete ret._v;
        }
    },
    timestamps : true
})

const Tournament = model<TournamentDoc, TournamentModel>('Tournament', TournamentSchema);

export default Tournament;