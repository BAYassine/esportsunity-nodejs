import { Router } from "express"

import { AuthRoutes } from "./AuthRoutes";
import { GameRoutes } from "./GameRoutes";
import { NewsRoutes } from "./NewsRoutes";
import { TeamRoutes } from "./TeamsRoutes";
import { TournamentRoutes } from "./TournamentRoutes";

const AdminRoutes = Router();

AdminRoutes.use('/teams', TeamRoutes);
AdminRoutes.use('/games', GameRoutes);
AdminRoutes.use('/login', AuthRoutes);
AdminRoutes.use('/news', NewsRoutes);
AdminRoutes.use('/tournaments', TournamentRoutes);

export default AdminRoutes;