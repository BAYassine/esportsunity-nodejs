import { Router, Response, Request } from "express";
import Game from "../../../models/games";
import formidable from 'formidable';
import { saveFile } from "../../../utils/functions";


const router = Router();

router.post('/create', (req: Request, res: Response) => {
    try {
        var form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            const gameAttrs = JSON.parse(<string>fields.game);
            gameAttrs.icon = saveFile(<formidable.File>files.icon, 'games/icons');
            gameAttrs.portrait = saveFile(<formidable.File>files.portrait, 'games/portraits');
            gameAttrs.cover = saveFile(<formidable.File>files.cover, 'games/covers');
            gameAttrs.transparent = saveFile(<formidable.File>files.transparent, 'games/transparents');
            const game = await Game.create(gameAttrs)
            res.send(game);
        });
    } catch(error) {
        res.status(400).send({ message : "Error creating game", error });
    }
})

export { router as CreateGameRouter };