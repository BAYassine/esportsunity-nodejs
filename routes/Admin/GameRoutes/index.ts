import { Router } from "express";
import { CreateGameRouter } from "./create";
import { ListGamesRouter } from "./list";

const GameRoutes = Router();

GameRoutes.use(CreateGameRouter);
GameRoutes.use(ListGamesRouter);

export { GameRoutes };