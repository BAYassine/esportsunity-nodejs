import { Request, Response, Router } from "express";
import News from "../../../models/news";

const router = Router();

router.post('/create', async (req: Request, res: Response) => {
    const news = req.body;
    news.category = "GAMES";
    news.createdBy = req.user._id;
    const result = await News.create(news)
    res.send(result);
});

export { router as CreateNewsRouter }