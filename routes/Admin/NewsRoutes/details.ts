import { Request, Response, Router } from "express";
import News, { NewsDoc } from "../../../models/news";

const router = Router();

router.get("/:news_id", (req: Request, res : Response) => {
    const { news_id } = req.params;
    News.findById(news_id, (err, news : NewsDoc) => {
        if(err || !news)
            res.status(400).send();
        else res.send(news)
    })
})

export { router as NewsDetailsRouter }