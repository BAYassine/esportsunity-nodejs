import { Router } from "express";
import passport from "passport";
import { CreateNewsRouter } from "./create";
import { NewsDetailsRouter } from "./details";
import { FeaturedNewsRouter } from "./featured";
import { ListNewsRouter } from "./list";
import { UpdateNewsRouter } from "./update";

const NewsRoutes = Router();

NewsRoutes.use(passport.authenticate('jwt', { session : false }), ListNewsRouter);
NewsRoutes.use(passport.authenticate('jwt', { session : false }), FeaturedNewsRouter);
NewsRoutes.use(passport.authenticate('jwt', { session : false }), CreateNewsRouter);
NewsRoutes.use(passport.authenticate('jwt', { session : false }), UpdateNewsRouter);
NewsRoutes.use(NewsDetailsRouter);

export { NewsRoutes };