import { Request, Response, Router } from "express";
import News, { NewsDoc } from "../../../models/news";

const router = Router();

router.get("/list", (req: Request, res : Response) => {
    const items = <any>req.query.items || 15;
    const page = <any>req.query.page;
    const query = News.find({ featured : false })
        .sort({ createdAt : -1 });
        
    if(page)
        query.skip((parseInt(page)-1)*items);
    query.limit(parseInt(items))
        .populate('createdBy')
    query.exec((err, news : NewsDoc[]) => {
        res.send(news)
    })
})

export { router as ListNewsRouter }