import { Request, Response, Router } from "express";
import News from "../../../models/news";

const router = Router();

router.put('/update', (req : Request, res: Response) => {
    const news = req.body;
    News.findByIdAndUpdate(news.id, news)
        .then(() => res.send())
        .catch(() => res.status(500).send());
})

export { router as UpdateNewsRouter }