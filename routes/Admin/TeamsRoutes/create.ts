import { Router, Response, Request } from "express";
import { body } from "express-validator";
import Team from "../../../models/team";

const router = Router();

router.post('/create',async (req: Request, res: Response) => {
    try {
        const teamAttrs = req.body;
        teamAttrs.logo = `assets/images/logos/teams/team-logo-${Math.round(Math.random()*4)+1}.jpg`;
        const team = await Team.create(teamAttrs)
        res.send(team);
    } catch(error) {
        res.status(400).send({ message : "Error creating team", error });
    }
})

const TeamModelValidations = [
    body('name')
        .not()
        .isEmpty()
]

export { router as CreateTeamRouter };