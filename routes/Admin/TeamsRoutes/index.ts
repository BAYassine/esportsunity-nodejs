import { Router } from "express";
import { CreateTeamRouter } from "./create";
import { ListTeamsRouter } from "./list";

const TeamRoutes = Router();

TeamRoutes.use(ListTeamsRouter)
TeamRoutes.use(CreateTeamRouter)

export { TeamRoutes };