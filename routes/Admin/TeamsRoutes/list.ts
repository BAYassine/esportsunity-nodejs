import { Router, Response, Request } from "express";
import passport from "passport";
import Team from "../../../models/team";

const router = Router();

router.get('/list', async (req: Request, res: Response) => {
    try {
        const { page = 1, count = 15 } : any = req.params;
        const teams = await Team.find()
            .skip((page-1)*count)
            .limit(count);
        res.send(teams);
    } catch(error) {
        res.status(400).send({ message : "Error fetching teams", error });
    }
});

export { router as ListTeamsRouter };