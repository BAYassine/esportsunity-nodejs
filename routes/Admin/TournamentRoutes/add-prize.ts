import { Router } from "express";
import Media from "../../../models/media";
import { PrizeSchema } from "../../../models/prize";
import Tournament from "../../../models/tournament";

const router = Router();

router.post('/:id/prizes', async (req, res) => {
    const { id } = req.params;
    const tournament = await Tournament.findById(id);
    if(!tournament){
        res.status(400).send("Could not find tournament");
        return;
    }
    Media.findOne({ name : req.body.photo })
        .then((media) => {
            media.used = true;
            media.save();
        });
    tournament.prizes.push(req.body)
    await tournament.save();
    res.send();
})

export { router as AddPrizeRoute }