import { Router } from "express";
import Tournament from "../../../models/tournament";

const router = Router();

router.post('/:id/stage', async (req, res) => {
    const { id } = req.params;
    const tournament = await Tournament.findById(id);
    if(!tournament){
        res.status(400).send("Could not find tournament");
        return;
    }
    tournament.stages.push(req.body)
    await tournament.save();
    res.send();
})

export { router as AddStageRoute }