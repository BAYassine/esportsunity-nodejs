import { Request, Response, Router } from "express";
import Tournament from "../../../models/tournament";

const router = Router();

router.post('/create', async (req : Request, res : Response) => {
    const tournamentProps = req.body;
    try {
        const tournament = await Tournament.create(tournamentProps);
        res.send(tournament);
    } catch (e) {
        res.status(500).send({ message : "Error occured while creating the tournament" });
    }
})

export { router as CreateTournamentRoute };