import { Router } from "express";
import Tournament from "../../../models/tournament";

const router = Router();

router.patch('/:id/stage/:stageId/current', async (req, res) => {
    const { id, stageId } = req.params;
    const tournament = await Tournament.findById(id);
    if(!tournament){
        res.status(400).send("Could not find tournament");
        return;
    }
    for(let stage of tournament.stages){
        stage.current = stage.id == stageId;
    }
    await tournament.save();
    res.send();
})

export { router as CurrentStageRoute }