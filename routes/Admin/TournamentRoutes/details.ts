import { Router } from "express";
import Tournament from "../../../models/tournament";

const router = Router();

router.get('/details/:id', (req, res) => {
    const { id } = req.params;
    Tournament.findById(id)
        .then(tournament => {
            if(tournament)
                res.send(tournament)
            else {
                res.status(400).send("Could not find tournament")
            }
        })
        .catch(() => res.status(500).send('error occured'))
})

export { router as TournamentDetailsRoute }