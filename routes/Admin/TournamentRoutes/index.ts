import { Router } from "express";
import passport from "passport";
import { AddPrizeRoute } from "./add-prize";
import { AddStageRoute } from "./add-stage";
import { CreateTournamentRoute } from "./create";
import { CurrentStageRoute } from "./current-stage";
import { TournamentDetailsRoute } from "./details";
import { ListTournamentsRoute } from "./list";
import { RemovePrizeRoute } from "./remove-prize";
import { RemoveStageRoute } from "./remove-stage";
import { UpdateTournamentRoute } from "./update";

const router = Router();

router.use(passport.authenticate('jwt', { session : false }), CreateTournamentRoute)
router.use(passport.authenticate('jwt', { session : false }), ListTournamentsRoute)
router.use(passport.authenticate('jwt', { session : false }), TournamentDetailsRoute)
router.use(passport.authenticate('jwt', { session : false }), UpdateTournamentRoute)
router.use(passport.authenticate('jwt', { session : false }), AddStageRoute)
router.use(passport.authenticate('jwt', { session : false }), RemoveStageRoute)
router.use(passport.authenticate('jwt', { session : false }), CurrentStageRoute)
router.use(passport.authenticate('jwt', { session : false }), AddPrizeRoute)
router.use(passport.authenticate('jwt', { session : false }), RemovePrizeRoute)

export { router as TournamentRoutes }