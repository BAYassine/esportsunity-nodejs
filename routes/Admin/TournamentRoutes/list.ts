import { Request, Response, Router } from "express";
import Tournament, { TournamentDoc } from "../../../models/tournament";

const router = Router();

router.get('/list', async (req : Request, res : Response) => {
    const items = <any>req.query.items || 15;
    const page = <any>req.query.page;
    const query = Tournament.find()
        .sort({ createdAt : -1 });
        
    if(page)
        query.skip((parseInt(page)-1)*items);
    query.limit(parseInt(items))
    query.exec((err, tournaments : TournamentDoc[]) => {
        if(err)
            res.status(500).send({ message : "Error occured while fetching tournaments" })
        else 
            res.send(tournaments)   
    })
})

export { router as ListTournamentsRoute }