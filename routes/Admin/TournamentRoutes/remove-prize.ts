import { Router } from "express";
import Tournament from "../../../models/tournament";

const router = Router();

router.delete('/:id/prizes/:prizeId', async (req, res) => {
    const { id, prizeId } = req.params;
    const tournament = await Tournament.findById(id);
    if(!tournament){
        res.status(400).send("Could not find tournament");
        return;
    }
    tournament.prizes = tournament.prizes.filter(p => p.id != prizeId)
    await tournament.save();
    res.send();
})

export { router as RemovePrizeRoute }