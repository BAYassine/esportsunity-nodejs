import { Router } from "express";
import Tournament from "../../../models/tournament";

const router = Router();

router.delete('/:id/stage/:stageId', async (req, res) => {
    const { id, stageId } = req.params;
    const { keep_order } = req.query;
    const tournament = await Tournament.findById(id);
    if(!tournament){
        res.status(400).send("Could not find tournament");
        return;
    }
    if(!keep_order){
        const stageIndex = tournament.stages.findIndex(s => s.id == stageId);
        for(let i = stageIndex; i < tournament.stages.length; i++){
            tournament.stages[i].order--;
        }
    }
    tournament.stages = tournament.stages.filter(s => s.id != stageId)
    await tournament.save();
    res.send();
})

export { router as RemoveStageRoute }