import { Router } from "express";
import Tournament from "../../../models/tournament";

const router = Router();

router.put('/update/:id', (req, res) => {
    const { id } = req.params;
    Tournament.findByIdAndUpdate(id, req.body)
        .then(() => res.send())
        .catch(() => res.status(500).send("Error occured"));
})

export { router as UpdateTournamentRoute }