import { Router } from "express";
import { LoginRouter } from "./login";
import { RegisterRouter } from "./register";

const AuthRoutes = Router();
AuthRoutes.use(RegisterRouter);
AuthRoutes.use(LoginRouter);

export default AuthRoutes;