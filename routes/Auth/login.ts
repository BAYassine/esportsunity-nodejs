import { Router } from "express";
import jwt from "jsonwebtoken";
import fs from 'fs'
import path from 'path'
import passport from 'passport'
import { AccountStates, LoginErrors } from "../../utils/constants";
const router = Router();

router.post(
    '/login',
    async (req, res, next) => {
      passport.authenticate(
        'local',
        async (err, user) => {
          try {
            if(err) return next(new Error('An error occurred.'));
            if (!user) {
              res.status(400).send({ error : LoginErrors.BAD_CREDENTIALS }); return;
            }
            else if(user.state == AccountStates.SUSPENDED) {
              res.status(400).send({ error : LoginErrors.ACCOUNT_SUSPENDED }); return;
            }
            else if(user.state == AccountStates.VERIFICATION) {
              res.status(400).send({ error : LoginErrors.ACCOUNT_VERIFICATION }); return;
            }
  
            req.login(
              user,
              { session: false },
              async (error) => {
                if (error) return next(error);
  
                
                const { JWT_TTL } = process.env;
                var privateKey = fs.readFileSync(path.resolve(__dirname, '../../jwt/jwtRS256.key'));
                const token = jwt.sign({ id : user.id, isAdmin : true }, privateKey, { expiresIn : JWT_TTL, algorithm : 'RS256' });
                const expiresIn = new Date(Date.now() + parseInt(JWT_TTL));
  
                return res.json({ id : user.id , token, expiresIn });
              }
            );
          } catch (error) {
            return next(error);
          }
        }
      )(req, res, next);
    }
)

export { router as LoginRouter }