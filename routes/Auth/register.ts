import { Router, Response, Request } from "express";
import Account from "../../models/account";
import bcrypt from "bcrypt";
import { AccountStates, SignupErrors } from "../../utils/constants";

const router = Router();


router.post('/register', async (req : Request, res : Response) => {
    const { email, username, password } = req.body;
    try {
        const accountExists = await Account.findOne({ $or : [{email : email.toLowerCase()}, {username : username.toLowerCase()}] });
        if(!accountExists){
            const salt = await bcrypt.genSalt(10)
            const hash = await bcrypt.hash(password, salt);
            const account = await Account.create({ username : username.toLowerCase(), email: email.toLowerCase(), password : hash, state : AccountStates.ACTIVE });
            res.send({ message : "Signed up successfully", data : account });
        } else {
            if(accountExists.get('email') == email)
                res.status(400).send({ message : "User registration failed", error : SignupErrors.EMAIL_USED })
            if(accountExists.get('username') == username)
                res.status(400).send({ message : "User registration failed", error : SignupErrors.USERNAME_USED })
        }
    } catch (e){
        res.send({ message : e.message });
    }
})

export { router as RegisterRouter }