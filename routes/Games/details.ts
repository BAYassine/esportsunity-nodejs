import { Router, Response, Request } from "express";
import passport from "passport";
import Game from "../../models/games";

const router = Router();

router.get('/details/:id', async (req: Request, res: Response) => {
    try {
        const { id } : any = req.params;
        const game = await Game.findOne({ _id : id })
        res.send(game);
    } catch(error) {
        res.status(400).send({ message : "Error fetching games", error });
    }
});

export { router as GameDetailsRouter };