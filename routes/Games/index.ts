import { Router } from "express";
import { GameDetailsRouter } from "./details";
import { ListGamesRouter } from "./list";

const GameRoutes = Router();

GameRoutes.use(ListGamesRouter);
GameRoutes.use(GameDetailsRouter);

export default GameRoutes;