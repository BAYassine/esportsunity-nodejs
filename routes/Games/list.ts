import { Router, Response, Request } from "express";
import passport from "passport";
import Game from "../../models/games";

const router = Router();

router.get('/list', async (req: Request, res: Response) => {
    try {
        const { page = 1, count = 15 } : any = req.params;
        const games = await Game.find()
            .skip((page-1)*count)
            .limit(count);
        res.send(games);
    } catch(error) {
        res.status(400).send({ message : "Error fetching games", error });
    }
});

export { router as ListGamesRouter };