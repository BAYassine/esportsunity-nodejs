import { Request, Response, Router } from "express";
import passport from "passport";
import { UploadRouter } from "./upload";

const router = Router();

router.use(passport.authenticate('jwt', { session : false }), UploadRouter);

export { router as MediaRoutes }