import { Request, Response, Router } from "express";
import formidable from 'formidable';
import Media from "../../models/media";
import { saveFile } from "../../utils/functions";
import mime from 'mime-types';

const router = Router();

router.post('/upload', (req: Request, res : Response) => {
    var form = new formidable.IncomingForm();
    form.parse(req, async (err, _, files) => {
        try{
            if(err){
                res.status(500).send({ message : "Failed to upload file" });
                return;
            }
            const filename = saveFile(<formidable.File>files.file, 'media');
            const media = await Media.create({ name : filename, type : mime.lookup(filename) })
            res.send(media)
        } catch (e){
            console.log(e);
            res.status(500).send(e)
        }
    });
})

export { router as UploadRouter }