import { Response, Router } from "express";
import News from "../../models/news";

const router = Router();

router.get('/featured', (_, res : Response) => {
    News.find({ 
            featured : true, publishedOn : { $lte  : new Date() } 
        })
        .sort({ publishedOn : -1 })
        .exec((err, news : any) => {
            if(err) {
                console.log(err);
                res.status(500).send();
                return;
            }
            res.send(news);
        });
    
});

export { router as FeaturedNewsRouter }

