import { Router } from "express";
// import passport from "passport";
import { NewsDetailsRouter } from "./details";
import { FeaturedNewsRouter } from "./featured";
import { ListNewsRouter } from "./list";

const NewsRoutes = Router();

NewsRoutes.use(FeaturedNewsRouter);
NewsRoutes.use(NewsDetailsRouter);
NewsRoutes.use(ListNewsRouter);

export { NewsRoutes };