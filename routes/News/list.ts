import { Response, Router } from "express";
import News, { NewsDoc } from "../../models/news";

const router = Router();

router.get("", (req, res : Response) => {

    const query = News.find({ 
            publishedOn : { $lte  : new Date() },
            featured : false
        })   
        .sort({ createdAt : -1 })
    const items = <any>req.query.items || 15;
    const page = <any>req.query.page;
        
    if(page)
        query.skip((parseInt(page)-1)*items);
        
    query.limit(parseInt(items))
    
    query.exec((err, news : NewsDoc[]) => {
        if(err) {
            console.log(err);
            res.status(500).send();
            return;
        }
        res.send(news)
    })
})

export { router as ListNewsRouter }