import { Request, Response, Router } from "express";
import formidable from 'formidable';
import Player from "../../models/player";
import { saveFile } from "../../utils/functions";

const router = Router();

router.post('/profile/change-photo', async (req: Request, res : Response) => {
    var form = new formidable.IncomingForm();
    const playerData = await Player.findOne({ account_id : req.user._id});
    form.parse(req, async (err, _, files) => {
        try{
            if(err){
                res.status(500).send({ message : "Failed to change photo" });
                return;
            }
            playerData.photo = saveFile(<formidable.File>files.photo, 'players/photos');
            const player = await Player.findOneAndReplace({ account_id : req.user._id}, playerData.toJSON(), { new : true });
            res.send(player)
        } catch (e){
            console.log(e);
            res.status(500).send(e)
        }
    });
})

export  { router as ChangePhotoRouter }