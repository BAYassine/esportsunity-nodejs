import { Router, Request, Response } from "express"
import Config, { ConfigDoc } from "../../models/player-config";

const router = Router();


router.post('/config/update', async (req : Request, res: Response) => {
    const ConfigAttrs : ConfigDoc = req.body;
    
    const ConfigData = await Config.findOne({ account_id : req.user._id});
    if(ConfigData) {
        ConfigAttrs.account_id = req.user._id;
        const config = await Config.findOneAndReplace({ account_id : req.user._id}, ConfigAttrs);
        res.send(config)
    } else {
        ConfigAttrs.account_id = req.user._id;
        const config = await Config.create(ConfigAttrs);
        res.send(config)
    }
})

export { router as ConfigUpdateRouter }