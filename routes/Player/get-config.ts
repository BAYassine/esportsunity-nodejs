import { Router, Request, Response } from "express"
import Config from "../../models/player-config";

const router = Router();

router.get('/config', async (req : Request, res: Response) => {
    const config = await Config.findOne({ account_id : req.user._id});
    res.send(config)
})


export { router as GetConfigRouter }