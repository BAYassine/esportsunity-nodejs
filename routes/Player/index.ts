import { Router } from "express";
import passport from "passport";
import { ChangeAvatarRouter } from "./change-avatar";
import { ChangePhotoRouter } from "./change-photo";
import { ConfigUpdateRouter } from "./config-update";
import { GetConfigRouter } from "./get-config";
import { PlayerTeamsRouter } from "./player-teams";
import { ProfileInfoRouter } from "./profile-info";
import { ProfileUpdateRouter } from "./update-profile";

const PlayerRoutes = Router();

PlayerRoutes.use(passport.authenticate('jwt', { session: false }), ProfileInfoRouter)
PlayerRoutes.use(passport.authenticate('jwt', { session: false }), ProfileUpdateRouter)
PlayerRoutes.use(passport.authenticate('jwt', { session: false }), ChangeAvatarRouter)
PlayerRoutes.use(passport.authenticate('jwt', { session: false }), ChangePhotoRouter)
PlayerRoutes.use(passport.authenticate('jwt', { session: false }), GetConfigRouter)
PlayerRoutes.use(passport.authenticate('jwt', { session: false }), ConfigUpdateRouter)
PlayerRoutes.use(passport.authenticate('jwt', { session: false }), PlayerTeamsRouter)

export default PlayerRoutes;