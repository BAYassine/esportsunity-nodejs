import { Router, Request, Response } from "express"
import Player from "../../models/player";
import TeamMember from "../../models/team-member";

const router = Router();


router.get('/teams', async (req : Request, res: Response) => {
    const player = await Player.findOne({ account_id : req.user._id})
    let teamsMemberships = await TeamMember.find({ player : player._id })
        .populate('team');
    res.send(teamsMemberships);
})

export { router as PlayerTeamsRouter }