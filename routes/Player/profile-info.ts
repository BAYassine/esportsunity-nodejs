import { Router, Request, Response } from "express"
import Player from "../../models/player";
import { AppData } from "../../utils/app-data";

const router = Router();

router.get('/profile', async (req : Request, res: Response) => {
    const playerData = await Player.findOne({ account_id : req.user._id});
    if(!playerData){
        res.send()
        return;
    }
    const social_links = [];

    for (const social_link of playerData.social_links) {
        social_links.push({
            id : social_link.id,
            social_network : AppData.social_networks.find(sn => sn.slug == social_link.social_network),
            link : social_link.link
        })
    }
    const player : any = {...playerData.toJSON()}
    player.social_links = social_links;
    res.send(player)
})


export { router as ProfileInfoRouter }