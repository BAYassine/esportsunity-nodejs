import { Router, Request, Response } from "express"
import Player, { PlayerDoc } from "../../models/player";

const router = Router();


router.post('/profile/update', async (req : Request, res: Response) => {
    const playerReqParams = req.body;
    const playerAttrs : PlayerDoc = { ...playerReqParams }
    const player_links = [];
    for(let player_link of playerReqParams.social_links){
        player_links.push({ link : player_link.link, social_network: player_link.social_network.slug });
    }
    playerAttrs.social_links = player_links;
    const oldPlayer = await Player.findOne({ account_id : req.user._id});
    if(oldPlayer) {
        playerAttrs.account_id = req.user._id;
        const player = await Player.findOneAndReplace({ account_id : req.user._id}, playerAttrs);
        res.send(player)
    } else {
        playerAttrs.account_id = req.user._id;
        const player = await Player.create(playerAttrs);
        res.send(player)
    }
})

export { router as ProfileUpdateRouter }