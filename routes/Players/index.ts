import { Router } from "express";
import { SearchPlayerRoute } from "./search";

const PlayersRoutes = Router();

PlayersRoutes.use(SearchPlayerRoute)

export default PlayersRoutes