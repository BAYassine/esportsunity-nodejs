import { Request, Response, Router } from "express";
import Player from "../../models/player";

const router = Router();

router.get('/search', (req: Request, res : Response) => {
    const limit = req.query.limit || 10;
    const nickname = req.query.nickname;
    const regex = new RegExp(`${nickname}`, 'i')
    Player.find({
        nickname : regex
    }).then((players) => {
        res.send(players)
    })
})

export { router as SearchPlayerRoute }