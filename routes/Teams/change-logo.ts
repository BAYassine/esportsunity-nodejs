import { Request, Response, Router } from "express";
import formidable from 'formidable';
import Player from "../../models/player";
import Team from "../../models/team";
import { saveFile } from "../../utils/functions";

const router = Router();

router.post("/:team_id/change-logo", async (req: Request, res : Response) => {
    var form = new formidable.IncomingForm();
    const {team_id} = req.params;
    const teamData = await Team.findOne({ captain_id : req.user._id, _id : team_id });
    if(!teamData) {
        res.status(400).send("Could not find team");
        return;
    }
    form.parse(req, async (err, _, files) => {
        try{
            if(err){
                res.status(500).send({ message : "Failed to change team logo" });
                return;
            }
            teamData.logo = saveFile(<formidable.File>files.logo, 'teams/logos');
            const team = await Team.findOneAndReplace({ _id : team_id }, teamData.toJSON(), { new : true });
            res.send(team)
        } catch (e){
            console.log(e);
            res.status(500).send(e)
        }
    });
})

export  { router as ChangeTeamLogoRouter }