import { Router, Request, Response } from "express"
import Player from "../../models/player";
import Team from "../../models/team";
import TeamMember from "../../models/team-member";
import mongoose from 'mongoose'

const router = Router();


router.get('/:team_id', async (req : Request, res: Response) => {
    const {team_id} = req.params;
    const team = await Team.findById(mongoose.Types.ObjectId(team_id))
    if(team)
        res.send(team);
    else {
        res.status(400).send('Could not find team with given id');
    }
})

export { router as TeamDetailsRouter }