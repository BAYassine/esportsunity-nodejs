import { Router } from "express";
import passport from "passport";
import { UpdateTeamRouter } from "./update-team";
import { TeamDetailsRouter } from "./details";
import { ChangeTeamLogoRouter } from "./change-logo";
import { TeamMembersRouter } from "./members";

const TeamsRoutes = Router();

TeamsRoutes.use(passport.authenticate('jwt', { session : false }), UpdateTeamRouter)
TeamsRoutes.use(passport.authenticate('jwt', { session : false }), ChangeTeamLogoRouter)
TeamsRoutes.use(passport.authenticate('jwt', { session : false }), TeamMembersRouter)
TeamsRoutes.use(passport.authenticate('jwt', { session : false }), TeamDetailsRouter)
// TeamsRoutes.use('/update', passport.authenticate('jwt', { session : false }), UpdateTeamRouter)

export default TeamsRoutes