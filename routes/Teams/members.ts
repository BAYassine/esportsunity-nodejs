import { Request, Response, Router } from "express";
import TeamMember from "../../models/team-member";
import mongoose from 'mongoose';
import Team from "../../models/team";

const router = Router();

router.get('/:team_id/members', async (req : Request, res: Response) => {
    const { team_id } = req.params;
    const team = await Team.findOne({ _id : team_id })
    const teamMembers = await TeamMember.find({ team : team._id })
        .populate('player');
    res.send(teamMembers);
})

export { router as TeamMembersRouter }; 