import { Router, Request, Response } from "express"
import Player from "../../models/player";
import Team from "../../models/team";
import TeamMember, { TeamMemberRoles } from "../../models/team-member";

const router = Router();


router.post('/update', async (req : Request, res: Response) => {
    const teamReqParams = req.body;
    const teamAttrs = { ...teamReqParams }
    if(teamAttrs.id != null){
        const oldTeam = await Team.findOne({ captain_id : req.user._id, _id : teamAttrs.id });
        if(oldTeam) {
            const team = await Team.findOneAndReplace({ captain_id : req.user._id}, teamAttrs);
            res.send(team)
        } else {
            res.status(400).send("Could not find team")
        }
    } else {
        const player = await Player.findOne({ account_id : req.user._id });
        teamAttrs.captain_id = req.user._id;
        teamAttrs.members = 1;
        const team = await Team.create(teamAttrs);
        await TeamMember.create({
            player,
            team,
            role : TeamMemberRoles.CAPTAIN,
            joinDate : new Date()
        })
        res.send(team)
    }
})

export { router as UpdateTeamRouter }