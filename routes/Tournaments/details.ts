import { Router } from "express";
import Tournament from "../../models/tournament";

const router = Router();

router.get('/details/:id', (req, res) => {
    const { id } = req.params;
    Tournament.findById(id)
        .then((tournament) => {
            if(tournament)  res.send(tournament);
            else res.status(400).send({ message : "Could not find Tournament with given id" })
        })
        .catch(() => res.status(500).send())
})

export { router as TournamentDetailsRoute }