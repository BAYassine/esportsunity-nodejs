import { Router } from "express";
import { TournamentDetailsRoute } from "../Admin/TournamentRoutes/details";
import { ListTournamentsRoute } from "./list";

const router = Router();

router.use(ListTournamentsRoute)
router.use(TournamentDetailsRoute)

export { router as TournamentRoutes }