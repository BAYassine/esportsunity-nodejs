import { Router } from "express";
import Tournament, { TournamentDoc } from "../../models/tournament";

const router = Router();

router.get('/list', (req, res) => {
    const items = <any>req.query.items || 15;
    const page = <any>req.query.page;
    const query = Tournament.find()
        .sort({ publishedOn : -1 });
        
    if(page)
        query.skip((parseInt(page)-1)*items);
    query.limit(parseInt(items))
        .populate('game')
    query.exec((err, news : TournamentDoc[]) => {
        res.send(news)
    })
})

export { router as ListTournamentsRoute }