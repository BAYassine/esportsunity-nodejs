import AdminRoutes from "./Admin/AdminRoutes";
import GameRoutes from "./Games";
import AuthRoutes from "./Auth";
import PlayerRoutes from "./Player";
import PlayersRoutes from "./Players";
import TeamsRoutes from "./Teams";
import { NewsRoutes } from "./News";
import { MediaRoutes } from "./Media";
import { TournamentRoutes } from "./Tournaments";

var express = require('express');
var router = express.Router();

/* GET home page. */
router.use('/admin', AdminRoutes);
router.use('/games', GameRoutes);
router.use('/auth', AuthRoutes);
router.use('/player', PlayerRoutes);
router.use('/players', PlayersRoutes);
router.use('/teams', TeamsRoutes);
router.use('/news', NewsRoutes);
router.use('/media', MediaRoutes);
router.use('/tournaments', TournamentRoutes);

export default router;
