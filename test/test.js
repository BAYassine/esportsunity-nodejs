const request = require('supertest');
const app = require('../dist/app');

describe('App', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect(/\s/, done);
  });
}); 
