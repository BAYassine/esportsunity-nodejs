import { Strategy } from 'passport-local';
import Account from '../models/account';
import StaffAccount from '../models/staff_account';
import bcrypt from "bcrypt";

const AdminStrategy = () => {
    return new Strategy(
        {
          usernameField: 'login',
          passwordField: 'password'
        },
        async function(username, password, done) {
          try {
              const user = await StaffAccount.findOne({
                $or : [{ email : username.toLowerCase()}, { username : username.toLowerCase()}]
              });
              if (!user) 
                return done(null, false, { message: 'Incorrect username.' });
              const validPassword = await bcrypt.compare(password, user.get('password'));
              if (!validPassword) {
                return done(null, false, { message: 'Incorrect password.' });
              }
              return done(null, user, { message: 'Logged in Successfully' });          
          } catch (e){
            return done(e);
          }
        }
    )
}

export default AdminStrategy;