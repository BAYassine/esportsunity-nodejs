

const { SERVER_URL } = process.env;

export const AppData = {
    social_networks : [
        {
            slug : "instagram",
            name : "Instagram",
            icon : `${SERVER_URL}/assets/images/social-networks/instagram.svg`,
            url : "www.instagram.com"
        },
        {
            slug : "facebook",
            name : "Facebook",
            icon : `${SERVER_URL}/assets/images/social-networks/facebook.svg`,
            url : "www.facebook.com"
        },
        {
            slug : "youtube",
            name : "Youtube",
            icon : `${SERVER_URL}/assets/images/social-networks/youtube.svg`,
            url : "www.youtube.com"
        },
        {
            slug : "steam",
            name : "Steam",
            icon : `${SERVER_URL}/assets/images/social-networks/steam.svg`,
            url : "www.steam.com"
        },
        {
            slug : "discord",
            name : "Discord",
            icon : `${SERVER_URL}/assets/images/social-networks/discord.svg`,
            url : "www.discord.com"
        },
        {
            slug : "twittter",
            name : "Twittter",
            icon : `${SERVER_URL}/assets/images/social-networks/twittter.svg`,
            url : "www.twittter.com"
        },
        {
            slug : "twitch",
            name : "Twitch",
            icon : `${SERVER_URL}/assets/images/social-networks/twitch.svg`,
            url : "www.twitch.com"
        }
    ]
}