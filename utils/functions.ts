import formidable from 'formidable';
import { v4 as uuidv4 } from 'uuid';
import fs from "fs";
import path from "path";

function saveFile(file: formidable.File, destination: string) {
    const oldpath = file.path;
    const ext = file.name.slice(file.name.lastIndexOf('.') + 1);
    const unique_name = `${uuidv4()}.${ext}`;
    const newpath = `/public/uploads/${destination}/${unique_name}`;
    fs.renameSync(oldpath, path.join(global.appRoot, newpath));
    return `/uploads/${destination}/${unique_name}`;
}

export {
    saveFile
}