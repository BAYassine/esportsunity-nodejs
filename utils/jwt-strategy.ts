import { Strategy , ExtractJwt } from 'passport-jwt';
import Account from '../models/account';
import StaffAccount from '../models/staff_account';
import * as fs from 'fs';

const JWTStrategy = () => {
    
    var privateKey = fs.readFileSync('jwt/jwtRS256.key.pub');
    var opts = {
        jwtFromRequest : ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey : privateKey,
    }
    return (new Strategy(opts, async function(jwt_payload, done) {
        try {
            const user = await Account.findOne({  _id : jwt_payload.id });
            const staff = await StaffAccount.findOne({ _id: jwt_payload.id});
            if(staff && !jwt_payload.isStaff)
                return done(null, false);
            if (user || staff) {
                return done(null, user || staff);
            } else {
                return done(null, false);
                // or you could create a new account
            }
        } catch (err){
            return done(err, false);
        }
    }))
}

export {JWTStrategy};