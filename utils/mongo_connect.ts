
import { connect } from 'mongoose';
import StaffAccount from '../models/staff_account';
import bcrypt from "bcrypt";

const { MONGODB_LOGIN, MONGODB_PASS } = process.env;

function configureMongo(){
    connect(`mongodb+srv://${MONGODB_LOGIN}:${MONGODB_PASS}@esportsunitycluster.ghume.mongodb.net/esportsunity?retryWrites=true&w=majority`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
    }).then(() => {
        console.log("Connected to MongoDB");
        initialize();
    });
}

async function initialize(){
    const user = await StaffAccount.findOne({ username : "admin" });
    if(user) return;
    
    const salt = await bcrypt.genSalt(10)
    const hash = await bcrypt.hash("Admin@123", salt);
    StaffAccount.create({
        username : "admin",
        password : hash,
        email : "yassine.benamar.1@esprit.tn",
        name : "Yassine Ben Amar",
        verified : true,
        locked : false,
        roles : ["SUPERADMIN"]
    })
}

export { configureMongo }